#******************************************
# Author: Brandon Mendrick
# Answer to question #12 on ProjectEuler.net
# Answer: 76576500
#******************************************

from math import sqrt

class PrimeFactor:
    """Finds the prime factorization of a number."""

    def __init__(self):
        self.maxPrime = 128
        self.primeSeive = [True] * self.maxPrime
        self.primeSeive[0] = False
        self.primeSeive[1] = False

        self._updatePrimes()

    def factor(self, n):
        """Factors the input number into its prime factors.

        Returns an ordered list of lists. Second level lists are pairs:
            [prime #, # of occurances]

        """

        # Making sure the number is not negative or zero
        if n <= 0:
            raise TypeError("factor() argument must be a positive integer")

        # Checking if this is a trivial case
        if n == 1:
            return [[1, 1]]

        # Checking if there are enough primes generated
        if (int(sqrt(n)) + 1) > self.maxPrime:
            # Current set of primes is too small. Need to expand.
            while (sqrt(n) + 1) > self.maxPrime:
                self.maxPrime *= 2

            self._updatePrimes()

        result = []

        # Looping through all of the primes
        for prime in self.primes:
            if n == 1:
                break

            count = 0
            while n % prime == 0:
                count += 1
                n /= prime

            if count > 0:
                result.append([prime, count])

        # Checking if the number itself was a prime
        if len(result) == 0:
            result.append([n, 1])

        return result

    def _updatePrimes(self):
        """Uses updated primeSeive length to generate a new set of primes."""
        # Making sure the prime seive is long enough
        if len(self.primeSeive) < self.maxPrime:
            # TODO: more efficient to delete and make new instead of extending?
            self.primeSeive += [True] * (self.maxPrime - len(self.primeSeive))

        self.primes = []
        for index, val in enumerate(self.primeSeive):
            # If the index is True then it is prime; remove all multiples
            if val is True:
                self.primeSeive[index * 2 :: index] = [False] * (((self.maxPrime - 1) // index) - 1)
                self.primes.append(index)

def triangleNumbers():
    """Generates the sequence of triangle numbers."""
    n = 1
    while True:
        if n % 1000 == 0:
            print(n)
        yield (n * (n + 1)) / 2
        n += 1

def main():
    # Setting up variables
    numbers = triangleNumbers()
    currentNum = 1
    currentNumFactors = 1
    factory = PrimeFactor()

    # Looping until we find a number with more than 500 divisors
    while currentNumFactors < 500:
        currentNum = next(numbers)
        result = factory.factor(currentNum)

        # Number of divisors is counted as described here:
        # https://www.math.upenn.edu/~deturck/m170/wk2/numdivisors.html
        currentNumFactors = 1
        for factor in result:
            currentNumFactors *= factor[1] + 1

    print(currentNum, currentNumFactors)

# handling main calling
if __name__ == '__main__':
	main()
