#******************************************
# Author: Brandon Mendrick
# Answer to question #9 on ProjectEuler.net
# Answer: 31875000
#******************************************

def main():
	possible = list([])

	# creating list of all possible sums that equal 1000
	for i in range(1, 1000):
		for j in range(i + 1, 1000):
			tmp = [1000 - i - j, j, i]
			possible.append(sorted(tmp))

	for x in possible:
		if x[0]**2 + x[1]**2 == x[2]**2:
			print x[0] * x[1] * x[2]
			return

# handling main calling
if __name__ == '__main__':
	main()
