#******************************************
# Author: Brandon Mendrick
# Answer to question #2 on ProjectEuler.net
# Answer: 4613732
#******************************************

def main():
	count = 2	# initializing count to contain the value 2
	fib0 = 1
	fib1 = 2

	# looping until we reach 4 million
	while fib1 < 4000000:
		# calculating the next fib number
		tmp = fib0
		fib0 = fib1
		fib1 = tmp + fib0

		# checking if the newly calculated number is even
		if fib1 % 2 == 0:
			count += fib1
	
	print count

# handling main calling
if __name__ == '__main__':
	main()
