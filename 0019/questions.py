#******************************************
# Author: Brandon Mendrick
# Answer to question #19 on ProjectEuler.net
# Answer: 171
#******************************************

daysPerMonth = {
	1: 31,	# Jan
	2: 28,	# Feb
	3: 31, 	# Mar
	4: 30,	# Apr
	5: 31,	# May
	6: 30,	# Jun
	7: 31,	# Jul
	8: 31,	# Aug
	9: 30,	# Sep
	10: 31,	# Oct
	11: 30, # Nov
	12: 31, # Dec
}

class DayOfWeek:
	"""Class to keep track of the day of the week.

	Uses simple modulo 7 math to maintain status of the day of the
	week.

	"""

	def __init__(self, day=0):
		self.day = day

	def add(self, rhs):
		"""Adds a number of days to the current day."""
		self.day = (self.day + rhs) % 7

	def isSunday(self):
		"""Checks if the current day is Sunday."""
		if self.day == 6:
			return True
		else:
			return False

def main():
	# Holders
	count = 0
	day = DayOfWeek(1)
	year = 1901

	# Looping until the end of times
	while year <= 2000:
		for month in range(1, 13):
			daysToIncrease = daysPerMonth[month]

			# Determining if this is a leap year
			if month == 2:
				if year % 4 == 0:
					if year % 100 == 0:
						if year % 400 == 0:
							daysToIncrease += 1
					else:
						daysToIncrease += 1

			# Increasing the day of the week
			day.add(daysToIncrease)
			if day.isSunday():
				# Note: this is sloppy because it will look at 01/01/01
				count += 1

		# Increasing the year
		year += 1

	print(count)


# handling main calling
if __name__ == '__main__':
	main()
