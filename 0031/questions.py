#******************************************
# Author: Brandon Mendrick
# Answer to question #31 on ProjectEuler.net
# Answer:
#******************************************

from itertools import product
from itertools import combinations_with_replacement as combos_w_replace

class CoinPurse:
	"""Class to keep track of the number of coins of different values.

	The value and number of coins supported is configured in the "coinValues"
	variable.

	Supports the equals (=), not equals (!=), and addition (+) operations.

	Can also call the sum() function to determine the value of coins

	"""

	# Have to use integers here because of issues with float arithmetic
	coinValues = [
		1,
		2,
		5,
		10,
		20,
		50,
		100,
		200
	]

	def __init__(self, *args):
		"""Initializes a CoinPurse.

		Arguments for initalization are given in order from lowest to highest
		value. Number of arguments may exceed the number of values listed in
		coinValues; all excess arguments will be ignored.

		Values are allowed to be negative!

		"""
		self.coinPurse = [0] * len(CoinPurse.coinValues)
		for index, value in enumerate(args):
			try:
				self.coinPurse[index] += value
			except IndexError:
				break

	def __add__(self, rhs):
		"""Adds two coin purses together (number of coins, not value)."""
		newPurse = [x + y for x, y in zip(self.coinPurse, rhs.coinPurse)]
		return CoinPurse(*newPurse)

	# Accounting for reverse case
	__radd__ = __add__

	def __mul__(self, rhs):
		"""Multiples the number of coins in the purse by the rhs."""
		if not isinstance(rhs, int):
			return TypeError

		newPurse = [x * rhs for x in self.coinPurse]
		return CoinPurse(*newPurse)

	# Accounting for reverse case
	__rmul__ = __mul__

	def __eq__(self, rhs):
		"""Checking for equality in the number of each type of coin."""
		# Throw it out if it is anything besides another instance of CoinPurse
		if not isinstance(rhs, CoinPurse):
			return NotImplemented

		# Checking if the instances are equal
		for i in range(len(CoinPurse.coinValues)):
			if self.coinPurse[i] != rhs.coinPurse[i]:
				return False

		return True

	def __neq__(self, rhs):
		"""Defining not equals to for Python 2.X compliance."""
		if ret is NotImplemented:
			return ret
		return not ret

	def __str__(self):
		tmp = " ".join([str(x) for x in self.coinPurse])
		return str(self.sum()) + ": " + tmp

	def sum(self):
		"""Returns the total value of the coins in the CoinPurse.

		Utilizes addition and the values given in the coinValues variable.

		"""
		total = 0
		for index, value in enumerate(self.coinPurse):
			total += CoinPurse.coinValues[index] * value

		return total

def permuteDouble(target, secondary_target=None):
	"""Returns a list of permutations for the double case.

	Double case includes jumps from 5 to 10. Simply considers all possible ways
	to doulbe items in the target.

	In cases where the previous operation was a permuteDoubleAndHalf, we need
	to deal with a special case. Ex for 5 to 10 we need to account for 5 * 2p.
	Therefore, you can optionally provide a secondary list of targets. All
	combinations with that list will also be tried.

	Returned list contains instances of CoinPurse with no duplicates.

	"""
	ret = []

	# Generating and check all possibilities
	for result in combos_w_replace(target, 2):
		tmp = sum(result, CoinPurse())
		if tmp not in ret:
			ret.append(tmp)

	# Checking if there is a second list
	if secondary_target is None:
		return ret

	# Need to deal with the secondary list
	# Checking that it is valid
	if ret[0].sum() % secondary_target[0].sum() != 0:
		# Secondary targets are not valid; return what is
		return ret

	# Determining multiplacation factor
	mult_factor = ret[0].sum() / secondary_target[0].sum()

	# Generating and checking possibilities for the secondary targets
	for result in combos_w_replace(secondary_target, mult_factor):
		tmp = sum(result, CoinPurse())
		if tmp not in ret:
			ret.append(tmp)

	return ret

def permuteDoubleAndHalf(high_target, low_target):
	"""Returns a list of permutations for the double and a half case.

	Double and a half case includes jumps from 2 to 5. Utilizes two inputs:
		high_target: the number which is being doubled
		low_target: the number which is half of the high_target

	There are two cases which have to be check here:
		1) 2 * high_target + low_target
		2) 5 * low_target

	Returned list contains instances of CoinPurse with no duplicates.

	"""
	ret = []

	# Generating a set of all of the high_target purses doubled
	doubled = [x + y for x, y in combos_w_replace(high_target, 2)]

	# Combining all possible doubled combinations with a "half" value
	for result in product(doubled, low_target):
		tmp = sum(result, CoinPurse())
		if tmp not in ret:
			ret.append(tmp)

	# Generating all cases for 5 * low_target
	for result in combos_w_replace(low_target, 5):
		tmp = sum(result, CoinPurse())
		if tmp not in ret:
			ret.append(tmp)

	return ret

def main():
	"""Determines the number of combinations of coins.

	Currently done somewhat manually when how it is instructed to permute the
	coin sets. I know this can be automate and generalized for any case. #TODO

	"""
	# Setting the initial coins
	combinations = {}
	combinations[1] = [CoinPurse(1)]
	combinations[2] = [CoinPurse(2), CoinPurse(0, 1)]

	# Determining 5p
	combinations[5] = permuteDoubleAndHalf(combinations[2], combinations[1])
	combinations[5].append(CoinPurse(0, 0, 1))
	print(len(combinations[5]))

	# Determining 10p
	combinations[10] = permuteDouble(combinations[5], combinations[2])
	combinations[10].append(CoinPurse(0, 0, 0, 1))
	print(len(combinations[10]))

	# Determining 20p
	combinations[20] = permuteDouble(combinations[10])
	combinations[20].append(CoinPurse(0, 0, 0, 0, 1))
	print(len(combinations[20]))

	# Determining 50p
	combinations[50] = permuteDoubleAndHalf(combinations[20], combinations[10])
	combinations[50].append(CoinPurse(0, 0, 0, 0, 0, 1))
	print(len(combinations[50]))

	# Determining 100p
	combinations[100] = permuteDouble(combinations[50], combinations[20])
	combinations[100].append(CoinPurse(0, 0, 0, 0, 0, 0, 1))
	print(len(combinations[100]))

	# Determining 200p
	combinations[200] = permuteDouble(combinations[100])
	combinations[200].append(CoinPurse(0, 0, 0, 0, 0, 0, 0, 1))
	print(len(combinations[200]))

	# for x in combinations[20]:
	# 	print(x)

# handling main calling
if __name__ == '__main__':
	main()
