#******************************************
# Author: Brandon Mendrick
# Answer to question #4 on ProjectEuler.net
# Answer: 906609
#******************************************

def main():
	largest = 0

	for i in range(100, 1000):
		for j in range(i, 1000):
			num = i * j

			# reversing the digits of the number
			rnum = int("".join(list(reversed(str(num)))))

			if num == rnum and num > largest:
				largest = num

	print(largest)

# handling main calling
if __name__ == '__main__':
	main()
