#******************************************
# Author: Brandon Mendrick
# Answer to question #18 on ProjectEuler.net
# Answer: 1074
#******************************************

def main():
	# Reading the triangle into memory
	triangle = []
	with open("triangle.txt", "r") as f:
		for line in f:
			triangle.append([int(x) for x in line.split(" ")])

	# Looping through all levels of the triangle
	while len(triangle) > 1:
		# Popping the bottom row
		bottom = triangle[-1]
		triangle = triangle[:-1]

		# Looping through the new bottom row
		for i in range(len(triangle[-1])):
			if bottom[i] > bottom[i + 1]:
				triangle[-1][i] += bottom[i]
			else:
				triangle[-1][i] += bottom[i + 1]

	print(triangle[0][0])

# handling main calling
if __name__ == '__main__':
	main()
