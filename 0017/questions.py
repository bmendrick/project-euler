#******************************************
# Author: Brandon Mendrick
# Answer to question #17 on ProjectEuler.net
# Answer: 21124
#******************************************

NumberOfLetters = {
	0: 0,	# zero
	1: 3,	# one
	2: 3,	# two
	3: 5,	# three
	4: 4,	# four
	5: 4,	# five
	6: 3,	# six
	7: 5,	# seven
	8: 5,	# eight
	9: 4,	# nine
	10: 3,	# ten
	11: 6,	# eleven
	12: 6,	# twelve
	13: 8,	# thirteen
	14: 8,	# fourteen
	15: 7,	# fifteen
	16: 7,	# sixteen
	17: 9,	# seventeen
	18: 8,	# eighteen
	19: 8,	# nineteen
	20: 6,	# twenty
	30: 6,	# thirty
	40: 5,	# forty
	50: 5,	# fifty
	60: 5,	# sixty
	70: 7,	# seventy
	80: 6,	# eighty
	90: 6,	# ninety
	"and": 3,
	"hundred": 7,
	"thousand": 8
}

def countLetters(num):
	count = 0
	needAnd = False
	num = str(num)

	# Removing the thousands place digit
	if len(num) == 4:
		count += NumberOfLetters[int(num[0])] + NumberOfLetters["thousand"]
		num = num[1:]

	# Removing the hundreds place digit
	if len(num) == 3:
		if int(num[0]) != 0:
			count += NumberOfLetters[int(num[0])] + NumberOfLetters["hundred"]
			num = num[1:]

			# Accounting for the "and"
			if int(num) != 0:
				count += NumberOfLetters["and"]

	# Handling the rest of the number
	if len(num) == 2:
		if int(num[0]) == 1:
			count += NumberOfLetters[int(num)]
			return count
		else:
			count += NumberOfLetters[int(num[0]) * 10]
			num = num[1:]

	if len(num) == 1:
		count += NumberOfLetters[int(num)]

	return count

def main():
	count = 0
	for x in range(1, 1001):
		# Doing this strangely for debuggin purposes.
		tmp = countLetters(x)
		count += tmp
		# print(str(x) + ": " + str(tmp))

	print(count)

# handling main calling
if __name__ == '__main__':
	main()
