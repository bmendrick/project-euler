#******************************************
# Author: Brandon Mendrick
# Answer to question #3 on ProjectEuler.net
# Answer: 6857
#******************************************

# Notes: I don't think this works for perfect squares
# since the returned value would be zero and not the
# actual largest factor

def main():
	num = 600851475143
	tmp = 2

	# checking that the number we are considering (tmp)
	# is still reasonable for the num we are at
	while tmp**2 < num:
		# checking if the number we are considering (tmp)
		# is a factor of num
		while num % tmp == 0:
			num /= tmp
		tmp += 1

	print num

# handling main calling
if __name__ == '__main__':
	main()
