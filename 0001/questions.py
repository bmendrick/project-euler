#******************************************
# Author: Brandon Mendrick
# Answer to question #1 on ProjectEuler.net
# Answer: 233168
#******************************************

def main():
	count = 0

	for x in range(1000):
		if (x % 3 == 0) or (x % 5 == 0):
			count = count + x

	print count

# handling main calling
if __name__ == '__main__':
	main()
