#******************************************
# Author: Brandon Mendrick
# Answer to question #33 on ProjectEuler.net
# Answer: 100
#******************************************

from math import sqrt

def genSequence():
    n = 11
    while n < 100:
        yield n

        # incrementing the value; skipping 10s
        n += 1
        if n % 10 == 0:
            n += 1

def genNumerator(denominator):
    seq = genSequence()
    n = next(seq)
    while n < denominator:
        yield n
        n = next(seq)

def checkFraction(num, denom):
    """Checking if a fraction is interesting.

    Calculates the result normally and then tries all of the possible
    calculations while cancelling one direction.

    """
    result = float(num) / denom
    num = str(num)
    denom = str(denom)

    new_result = -1
    if num[0] == denom[0]:
        new_result = float(int(num[1])) / int(denom[1])
    elif num[0] == denom[1]:
        new_result = float(int(num[1])) / int(denom[0])
    elif num[1] == denom[0]:
        new_result = float(int(num[0])) / int(denom[1])
    elif num[1] == denom[1]:
        new_result = float(int(num[0])) / int(denom[0])

    if new_result == result:
        return result

    return None

def prime_seive(n):
    primes = [True] * n
    primes[0] = False
    primes[1] = False
    results = []

    for index, val in enumerate(primes):
        # Checking if index is True, removing multiples if so
        if val is True:
            primes[index * 2 :: index] = [False] * (((n - 1) // index) - 1)
            results.append(index)

    return results

def main():
    result_num = 1
    result_denom = 1
    denominator = genSequence()
    for denom in denominator:
        numerator = genNumerator(denom)
        for num in numerator:
            # Looping through all possible numerators and denominators
            # Validity of the fraction is checked by the generating function
            if checkFraction(num, denom) is not None:
                # If the fraction is interesting, multiply into the result nums
                result_num *= num
                result_denom *= denom

    # Generating a list of primes
    if result_num > result_denom:
        primes = prime_seive(int(sqrt(result_num)) + 1)
    else:
        primes = prime_seive(int(sqrt(result_denom)) + 1)

    # Reducing the fraction
    for prime in primes:
        while result_num % prime == 0 and result_denom % prime == 0:
            result_num /= prime
            result_denom /= prime

    print(result_num, result_denom)

# handling main calling
if __name__ == '__main__':
	main()
