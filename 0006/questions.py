#******************************************
# Author: Brandon Mendrick
# Answer to question #6 on ProjectEuler.net
# Answer: 25164150
#******************************************

def main():
	sum_squares = 0
	square_sum = 0
	for i in range(1, 101):
		sum_squares += i**2
		square_sum += i

	square_sum = square_sum**2

	print square_sum - sum_squares

# handling main calling
if __name__ == '__main__':
	main()
