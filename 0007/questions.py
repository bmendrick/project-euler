#******************************************
# Author: Brandon Mendrick
# Answer to question #7 on ProjectEuler.net
# Answer: 104743
#******************************************

def is_prime(num, primes):
	for prime in primes:
		if num % prime == 0:
			return False
	
	primes.append(num)
	return True

def main():
	max_num = 1000000
	nth_prime = 10001
	count = 1
	primes = list([2])

	for i in range(3, max_num, 2):
		if is_prime(i, primes):
			count += 1
		
		if count == nth_prime:
			print primes[-1]
			break

	if count != nth_prime:	
		print "Max not large enough for specified nth prime"

# handling main calling
if __name__ == '__main__':
	main()
