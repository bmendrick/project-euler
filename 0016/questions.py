#******************************************
# Author: Brandon Mendrick
# Answer to question #16 on ProjectEuler.net
# Answer: 1366
#******************************************

def main():
	count = 0
	number = 2 ** 1000
	for x in str(number):
		count += int(x)

	print(count)

# handling main calling
if __name__ == '__main__':
	main()
