#******************************************
# Author: Brandon Mendrick
# Answer to question #20 on ProjectEuler.net
# Answer: 648
#******************************************

from math import factorial

def main():
	count = 0
	for x in str(factorial(100)):
		count += int(x)

	print(count)

# handling main calling
if __name__ == '__main__':
	main()
