#******************************************
# Author: Brandon Mendrick
# Answer to question #10 on ProjectEuler.net
# Answer: 142913828922
#******************************************

# Thank you to jasonbhill's blog for showing me this concept
def prime_seive(n):
	count = 0
	primes = [True] * n
	primes[0], primes[1] = [None], [None]

	for index, val in enumerate(primes):
		# checking if an index it True,
		# if so, then it is prime and we remove all its multiples
		if val is True:
			primes[index * 2 :: index] = [False] * (((n - 1)//index) - 1)
			count += index

	return count

def main():
	max_num = 2000000

	total = prime_seive(max_num)

	print total

# handling main calling
if __name__ == '__main__':
	main()
