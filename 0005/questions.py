#******************************************
# Author: Brandon Mendrick
# Answer to question #5 on ProjectEuler.net
# Answer: 232792560
#******************************************

def main():
	# multiplying all the primes between 1 and 20 we get 9699690
	# therefore, we can start looking from the next multiple of 20
	# we are also counting by 20s to remove a modulo computation
	current_num = 9699700
	
	while True:
		# sentinel
		works = True
		
		# all multiples are covered in the range 11 to 19
		for i in range(11, 20):
			if current_num % i != 0:
				works = False
				break

		if works:
			print current_num
			break
		else:
			current_num += 20

# handling main calling
if __name__ == '__main__':
	main()
