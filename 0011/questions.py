#******************************************
# Author: Brandon Mendrick
# Answer to question #11 on ProjectEuler.net
# Answer: 70600674
#******************************************

import operator

class GridSearch:
    """Customizable 2D (grid) searching algorithm.

    GridSearch is a configurable algorithm which looks through 2D arrays (grids)
    in order to find the "best" result. The default configuration will search
    the grid in order to find a series of 4 numbers, vertically, horizontally,
    or diagnolly, who's product is the largest.

    The class takes the following initialization parameters:
        grid - (mandatory) The 2D array which will be searched
        operation - lambda function which takes two inputs
        comparison - function or operator which will be used to compare two
            results and make a determination which is "better"
        horizontal - boolean to determine if horizontal searching is allowed
        vertical - boolean to determine if vertical searching is allowed
        diagnol - boolean to determine if diagnol searching is allowed
        distance - integer for the number of items in the grid to group at a time

    """

    def __init__(self, grid, operation=lambda x,y: x * y,
                comparison=operator.gt, horizontal=True,
                vertical=True, diagnol=True, distance=4):
        self.grid = grid

        self.op = operation
        self.comp = comparison

        self.directions = {}
        if horizontal:
            self.directions["horizontal"] = self._computeHorizontal
        if vertical:
            self.directions["vertical"] = self._computeVertical
        if diagnol:
            self.directions["diag_left"] = self._computeLDiag
            self.directions["diag_right"] = self._computeRDiag

        self.distance = distance

    def search(self):
        """Searches through the grid based on the input settings."""
        self.largest = {"x": -1, "y": -1, "num": None, "dir": None}

        for x, row in enumerate(self.grid):
            for y, number in enumerate(row):
                # Looping through each number within the grid
                for dir_, func in self.directions.iteritems():
                    # Looping through all configured directions
                    result = func(x, y)
                    if self.comp(result, self.largest["num"]):
                        # We found a "better" result; set new "largest"
                        self.largest["num"] = result
                        self.largest["x"] = x
                        self.largest["y"] = y
                        self.largest["dir"] = dir_

        return self.largest

    def _computeHorizontal(self, x, y):
        """Compute a horizontal segment starting from (x,y)."""
        xfunc = lambda n,i: n
        yfunc = lambda n,i: n + i
        return self._compute(xfunc, yfunc, x, y)

    def _computeVertical(self, x, y):
        """Compute a vertical segment starting from (x,y)."""
        xfunc = lambda n,i: n + i
        yfunc = lambda n,i: n
        return self._compute(xfunc, yfunc, x, y)

    def _computeLDiag(self, x, y):
        """Compute a downward, left diagnol segment starting from (x,y)."""
        xfunc = lambda n,i: n + i
        yfunc = lambda n,i: n - i
        return self._compute(xfunc, yfunc, x, y)

    def _computeRDiag(self, x, y):
        """Compute a downward, right diagnol segment starting from (x,y)."""
        xfunc = lambda n,i: n + i
        yfunc = lambda n,i: n + i
        return self._compute(xfunc, yfunc, x, y)

    def _compute(self, xfunc, yfunc, x, y):
        """Computes a segment of the grid.

        The function uses (x,y) as a starting point on the grid. It performs
        the xfunc and yfunc on those coordinates respectively to move along
        the grid self.distance number of times. Once all number are determined
        in the path, it uses the self.op function to compute a final result.

        This will return None if any of the coordinates go outside of the grid.

        """
        numbers = []

        # Determining if this is a valid path and gathering the numbers
        try:
            for i in range(self.distance):
                numbers.append(self.grid[xfunc(x, i)][yfunc(y, i)])
        except IndexError:
            return None

        # Determing the result
        if self.distance > 1:
            result = self.op(numbers[0], numbers[1])
            for i in range(2, self.distance):
                result = self.op(result, numbers[i])
            return result
        else:
            return numbers[0]

def main():
    # Setting up a variable with the grid inside of it
    grid = []
    with open("grid.txt", "r") as f:
        for row in f:
            row = row.split(" ")
            row = [int(x, 10) for x in row]
            grid.append(row)

    # Searching the grid
    searcher = GridSearch(grid)
    print(searcher.search())

# handling main calling
if __name__ == '__main__':
	main()
