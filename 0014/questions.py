#******************************************
# Author: Brandon Mendrick
# Answer to question #14 on ProjectEuler.net
# Answer: 837799
#******************************************

def countCollatz(n):
	"""Simple counter for Collatz's Conjecture."""
	count = 0
	while n > 1:
		if n % 2 == 0:
			n /= 2
		else:
			n = 3 * n + 1

		count += 1

	return count

def main():
	largest_num = 0
	largest_count = 0
	for x in range(1,1000000):
		current = countCollatz(x)
		if current > largest_count:
			largest_count = current
			largest_num = x

	print(largest_num)

# handling main calling
if __name__ == '__main__':
	main()
